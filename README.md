# Advanced Ebuild Tool

<p align="center">
    <a href="https://gitlab.com/xgqt/aet/pipelines">
        <img src="https://gitlab.com/xgqt/aet/badges/master/pipeline.svg">
    </a>
    <a href="https://gitlab.com/xgqt/aet/commits/master.atom">
        <img src="https://img.shields.io/badge/feed-atom-orange.svg">
    </a>
    <a href="./LICENSE">
        <img src="https://img.shields.io/badge/license-ISC-blue.svg">
    </a>
</p>

APT-like wrapper for portage with helpful additions


# About

## Current AET options

- list - list packages based on package names
- search - search in package descriptions
- show - show package details
- script - show the ebuild script of a package
- install - install packages
- reinstall - reinstall packages
- remove - remove packages
- fetch - fetch package sources
- autoremove - remove automatically all unused packages
- clean - remove obsolete packages
- update - update list of available packages
- upgrade - upgrade the system by installing/upgrading packages
- full-upgrade - upgrade the system by removing/installing/upgrading packages
- edit-sources - edit the source information file
- edit-world - edit the world set


# Installation

# Git

As root:

```sh
mkdir -p /opt/genlica
cd /opt/genlica
git clone --recursive --verbose https://gitlab.com/xgqt/aet
cd aet
sudo make install
```


# Gentoo

As root:

```sh
emerge -1nv app-eselect/eselect-repository
eselect repository enable myov
emaint sync -r myov
emerge -av --autounmask app-portage/aet
```
